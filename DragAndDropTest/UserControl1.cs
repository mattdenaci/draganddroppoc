﻿using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace DragAndDropTest
{
    public partial class UserControl1 : UserControl
    {
        public UserControl1()
        {
            InitializeComponent();
            AllowDrop = true;
            newButton.AllowDrop = true;
            newButton.DragDrop += ButtonDragDrop;
            newButton.DragEnter += ButtonDragEnter;
            newButton.MouseDown += (sender, args) => newButton.DoDragDrop(newButton.Text, DragDropEffects.Copy);
        }


        public void AddButtion(SimpleButton newButtion)
        {
            var item1 = Level1.AddItem();
            item1.TextVisible = false;

            var nextNewButton = new SimpleButton
            {
                AllowDrop = true,
                MinimumSize = new Size(120, 42),
                MaximumSize = new Size(120, 42)
            };
            newButton.AllowDrop = true;
            nextNewButton.DragDrop += ButtonDragDrop;
            nextNewButton.DragEnter += ButtonDragEnter;
            item1.Control = nextNewButton;
        }

        private static void ButtonDragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        private void ButtonDragDrop(object sender, DragEventArgs e)
        {
            var button = sender as SimpleButton;
            if (String.IsNullOrWhiteSpace(button.Text))
            {
                var item1 = Level1.AddItem();
                item1.TextVisible = false;
                var nextNewButton = new SimpleButton
                {
                    AllowDrop = true,
                    MinimumSize = new Size(120, 42),
                    MaximumSize = new Size(120, 42)
                };
                nextNewButton.AllowDrop = true;
                nextNewButton.DragDrop += ButtonDragDrop;
                nextNewButton.DragEnter += ButtonDragEnter;
                nextNewButton.MouseDown += (snder, args) => nextNewButton.DoDragDrop(newButton.Text, DragDropEffects.Copy);

                item1.Control = nextNewButton;
                Level1.AddItem(item1);
            }
            button.Text = e.Data.GetData(DataFormats.Text).ToString();

        }
}
}

