﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Filtering;
using DevExpress.XtraGrid;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using StyleFormatCondition = DevExpress.XtraTreeList.StyleFormatConditions.StyleFormatCondition;
using StyleFormatConditionCollection = DevExpress.XtraTreeList.StyleFormatConditions.StyleFormatConditionCollection;

namespace DragAndDropTest
{
    public partial class MainForm : XtraForm
    {
        public SimpleButton SimpleButton { get; set; }

        public List<DataClass> TreeList2DataSource { get; set; }

        public Dictionary<int, LevelManager> LevelManagers = new Dictionary<int, LevelManager>();

        public Cursor NoDropCursor { get; set; }

        public Cursor DropOkCursor { get; set; }

        public DataClass SelectedDataClass { get; set; }

        public MainForm()
        {
            InitializeComponent();
            SetupDataSet(DataSet1TreeList, DataSet1Source, "Portfolio", Color.FromArgb(169, 217, 216));
            SetupDataSet(DataSet2TreeList, DataSet2Source, "Investment", Color.FromArgb(206, 235, 201));
            SetupReportDesignerTreeList();
            TreeList2DataSource = new List<DataClass>();
        }

        public List<DataClass> DataSet1Source
        {
            get
            {
                var dataClassList = new List<DataClass>{
                new DataClass
            {
                Name = "Portfolio Code",
                Caption = "Portfolio",
                Id = 123214,
                IsVisible = true,
                AssociatedDataSet = "Portfolio"
            },
                
                 new DataClass
                {
                    Name = "YTD P&L Total",
                    Caption = "Year to Date P&L",
                    Id = 324234,
                    IsVisible = false,
                    AssociatedDataSet = "Portfolio"
                },
                new DataClass
                {
                    Name = "QTD P&L Total",
                    Caption = "Quarter to Date P&L",
                    Id = 324234,
                    IsVisible = false,
                    AssociatedDataSet = "Portfolio"
                },
                new DataClass
                {
                    Name = "MTD P&L Total",
                    Caption = "Month to Date P&L",
                    Id= 1324234,
                    IsVisible = true,
                    AssociatedDataSet = "Portfolio"
                },
                new DataClass
                {
                    Name = "DTD P&L Total",
                    Caption = "Daily P&L",
                    Id = 324234,
                    IsVisible = false,
                    AssociatedDataSet = "Portfolio"
                }
            };

                return dataClassList;
            }
        }

        private void SetupDataSet(TreeList treeList, List<DataClass> source, string dataSetName, Color backColor)
        {
            treeList.MouseDown += TreeViewMouseDown;
            treeList.GiveFeedback += UpdateDragCursor;
            AddItemsToTreeList(treeList, source, dataSetName, backColor);
        }


        private void SetupReportDesignerTreeList()
        {
            ReportDesignerTreeList.AllowDrop = true;
            ReportDesignerTreeList.DragEnter += ButtonDragEnter;
            ReportDesignerTreeList.DragDrop += TreeListDragDrop;
            ReportDesignerTreeList.Click += UpdatePropertyGrid;
            ReportDesignerTreeList.ShownEditor += DisableEditingExceptForHeaderColumn;

            ReportDesignerTreeList.OptionsView.ShowRoot = false;

            ReportDesignerTreeList.DragObjectDrop += UpdateColumnOrder;

            SetupReadonlyColumnAndHeaderRow();

            var styleFormatCondition = new StyleFormatCondition
                (
                    FormatConditionEnum.Equal,
                    ReportDesignerTreeList.Columns[0], null,
                    "Investment"
                );
            styleFormatCondition.Appearance.BackColor = Color.FromArgb(206, 235, 201);
            //styleFormatCondition.Appearance.ForeColor = Color.FromArgb(206, 235, 201);
            styleFormatCondition.ApplyToRow = true;
            ReportDesignerTreeList.FormatConditions.Add(styleFormatCondition);

            var styleFormatCondition2 = new StyleFormatCondition
                (
                    FormatConditionEnum.Equal,
                    ReportDesignerTreeList.Columns[0], null,
                    "Portfolio"
                );
            styleFormatCondition2.Appearance.BackColor = Color.FromArgb(169, 217, 216); 
            styleFormatCondition2.ApplyToRow = true;
            ReportDesignerTreeList.FormatConditions.Add(styleFormatCondition2);




        }

        public Cursor GetNoDragCursor(DataClass data)
        {
            var textEdit = MakeTextEditForCursor(data);
            var bitMapOfTextEdit = MakeBitMapOfTextEdit(textEdit);
            AddNoCursorToBitMap(bitMapOfTextEdit, textEdit);

            bitMapOfTextEdit.MakeTransparent(Color.White);
            
            AddArrowCursorToBitmap(bitMapOfTextEdit, textEdit);
            var cursorOffset = new Point(textEdit.Size.Width / 2, textEdit.Size.Height);
            return CreateCursor(bitMapOfTextEdit, cursorOffset);
        }



        private Cursor GetDropOkCursor(DataClass data)
        {
            var textEdit = MakeTextEditForCursor(data);
            var bitMapOfTextEdit = MakeBitMapOfTextEdit(textEdit);

            bitMapOfTextEdit.MakeTransparent(Color.White);

            AddArrowCursorToBitmap(bitMapOfTextEdit, textEdit);
            
            var cursorOffset = new Point(textEdit.Size.Width / 2, textEdit.Size.Height);
            return CreateCursor(bitMapOfTextEdit, cursorOffset);
        }

        private static void AddNoCursorToBitMap(Bitmap bitMapOfTextEdit, TextEdit textEdit)
        {
            Graphics gridBitmapAsGraphics = Graphics.FromImage(bitMapOfTextEdit);
            Cursors.No.Draw(gridBitmapAsGraphics, new Rectangle((textEdit.Size.Width / 2) + 10, textEdit.Size.Height * 2, (textEdit.Size.Width / 2) + 60, textEdit.Size.Height + 60));
        }

        private static void AddArrowCursorToBitmap(Bitmap bitMapOfTextEdit, TextEdit textEdit)
        {
            Graphics newBitmapAsGraphics = Graphics.FromImage(bitMapOfTextEdit);
            Cursors.Arrow.Draw(newBitmapAsGraphics, new Rectangle(textEdit.Size.Width / 2, textEdit.Size.Height + 3, (textEdit.Size.Width / 2) + 20, textEdit.Size.Height + 20));
        }

        private static Bitmap MakeBitMapOfTextEdit(TextEdit textEdit)
        {
            var imageBounds = new Rectangle(new Point(0, 0), new Size(textEdit.Size.Width + 20, textEdit.Size.Height * 4));
            var textEditBitMap = new Bitmap(imageBounds.Width, imageBounds.Height);

            textEdit.DrawToBitmap(textEditBitMap, imageBounds);

            return textEditBitMap;
        }

        private TextEdit MakeTextEditForCursor(DataClass data)
        {
            var textBox = new TextEdit {EditValue = data.Name};
            textBox.Size = textBox.CalcBestSize();
            textBox.BackColor = data.AssociatedDataSet == "Portfolio"
                    ? Color.FromArgb(169, 217, 216)
                    : Color.FromArgb(206, 235, 201);
            return textBox;
        }

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetIconInfo(IntPtr hIcon, ref IconInfo pIconInfo);
        [DllImport("user32.dll")]
        public static extern IntPtr CreateIconIndirect(ref IconInfo icon);

        public struct IconInfo
        {
            public bool fIcon;
            public int xHotspot;
            public int yHotspot;
            public IntPtr hbmMask;
            public IntPtr hbmColor;
        }

        public static Cursor CreateCursor(Bitmap bmp, Point hotspot)
        {
            if (bmp == null) return Cursors.Default;
            IntPtr ptr = bmp.GetHicon();
            IconInfo tmp = new IconInfo();
            GetIconInfo(ptr, ref tmp);
            tmp.fIcon = false;
            tmp.xHotspot = hotspot.X;
            tmp.yHotspot = hotspot.Y;
            ptr = CreateIconIndirect(ref tmp);
            return new Cursor(ptr);
        }


        private void DisableEditingExceptForHeaderColumn(object sender, EventArgs e)
        {

            UpdatePropertyGrid(sender, e);
            var senderAsTreeList = sender as TreeList;
            if (senderAsTreeList == null)
                return;
            
            senderAsTreeList.ActiveEditor.Properties.ReadOnly = senderAsTreeList.FocusedNode.Id != 0;
        }


        private void UpdatePropertyGrid(object sender, EventArgs e)
        {
            var tree = sender as TreeList;
            if (tree == null)
                return;
            var info = tree.CalcHitInfo(tree.PointToClient(MousePosition));
            if(info.HitInfoType == HitInfoType.Cell)
                propertyGridControl1.SelectedObject = info.Node.GetValue(info.Column);

        }


        private void SetupReadonlyColumnAndHeaderRow()
        {
            var addField = ReportDesignerTreeList.Columns.Add();
            addField.FieldName = "  ";
            addField.Visible = true;
            addField.VisibleIndex = 1;
            addField.Fixed = FixedStyle.Left;
            addField.OptionsColumn.AllowMove = false;
            

            var dataSetName = ReportDesignerTreeList.Columns.Add();
            dataSetName.FieldName = "  ";
            dataSetName.Visible = true;
            dataSetName.VisibleIndex = 1;
            


            
            ReportDesignerTreeList.AppendNode(new object[]{"Column Headers:",}, -1);
            LevelManagers.Add(0, new LevelManager(0)
            {
                AssociatedDataSet = "None"
            });
            
            
            ReportDesignerTreeList.AppendNode(new object[]{" ", "Add Field"}, -1);

            ReportDesignerTreeList.Columns[0].OptionsColumn.AllowSort = false;
            ReportDesignerTreeList.Columns[0].OptionsColumn.AllowMove = false;
            ReportDesignerTreeList.Columns[0].OptionsColumn.AllowEdit = false;

            ReportDesignerTreeList.Columns[1].OptionsColumn.AllowSort = false;
            ReportDesignerTreeList.Columns[1].OptionsColumn.AllowMove = false;
            ReportDesignerTreeList.Columns[1].OptionsColumn.AllowEdit = false;

            ReportDesignerTreeList.Columns[0].Visible = false;
 

            LevelManagers.Add(1, new LevelManager(1));
            

        }

        private void UpdateColumnOrder(object sender, DragObjectDropEventArgs e)
        {
            Debug.Write("Update column order fired");
            Debug.Write(ReportDesignerTreeList.Nodes.Count);
            var lastNode = ReportDesignerTreeList.Nodes[ReportDesignerTreeList.Nodes.Count - 1];
                
            //ReportDesignerTreeList.Nodes.Remove(lastNode);
            //ReportDesignerTreeList.AppendNode(new object[] { " ", "Add Field" }, null);
        }

        

        private void TreeListDragDrop(object sender, DragEventArgs e)
        {
            

            var data= e.Data.GetData(typeof(DataClass));
            var dataAsDataClass = data as DataClass;
            if (dataAsDataClass == null)
                return;

            
            var list = (TreeList)sender;
            var info = list.CalcHitInfo(list.PointToClient(new Point(e.X, e.Y)));

            if (info.Node == null || info.Node.GetValue(info.Column) == null)
                return;

            if (!LevelManagers[info.Node.Id].FieldCanBeAddedToLevel(dataAsDataClass))
                return;

            

            if (UserDidNotClickOnAddingCell(info))
            {
                info.Node.SetValue(info.Column, data);
                return;
            }
                

            if (info.Node == null)
                return;

            ReportDesignerTreeList.Columns[0].Visible = true;

            var newColumnIndex = GetFirstColumnThatDoesntHaveData(info.Node);

 
            
            info.Node.SetValue(newColumnIndex, data);
            
            
            ReportDesignerTreeList.Columns[newColumnIndex].OptionsColumn.AllowMove = true;
            ReportDesignerTreeList.Columns[newColumnIndex].OptionsColumn.AllowEdit = false;
            ReportDesignerTreeList.Columns[newColumnIndex].Fixed = FixedStyle.None;


            propertyGridControl1.SelectedObject = data;

            if (newColumnIndex == ReportDesignerTreeList.Columns.Count - 1)
            {
                FillInHeaderWithFieldCaption(dataAsDataClass);


                var addCol = ReportDesignerTreeList.Columns.Add();
                addCol.Visible = true;
                addCol.OptionsColumn.AllowSort = false;
                addCol.OptionsColumn.AllowMove = false;
                addCol.OptionsColumn.AllowEdit = false;
                addCol.OptionsColumn.ReadOnly = true;
                addCol.Fixed = FixedStyle.Right;

            
                info.Node.SetValue(addCol.AbsoluteIndex, "Add Field");
            }
            else
            {
                info.Node.SetValue(newColumnIndex+1, "Add Field");
            }

            info.Node.SetValue(0, dataAsDataClass.AssociatedDataSet);

            if(info.Node.Id == ReportDesignerTreeList.Nodes.Count-1)
                ReportDesignerTreeList.AppendNode(new object[] {" ", "Add Field" }, -1);
            LevelManagers[ReportDesignerTreeList.Nodes.Count - 1] = new LevelManager(ReportDesignerTreeList.Nodes.Count - 1);

            UpdateAddLevelColumns();

        }

        private void FillInHeaderWithFieldCaption(DataClass data)
        {
            ReportDesignerTreeList.Nodes[0].SetValue(ReportDesignerTreeList.Columns.Count - 1, data.Caption);
        }

        private void UpdateAddLevelColumns()
        {
            foreach (TreeListNode node in ReportDesignerTreeList.Nodes)
            {
                if (node.Id == ReportDesignerTreeList.Nodes.Count - 1)
                    break;

                var nodeValue = node.GetValue(ReportDesignerTreeList.Columns.Count - 2) as String;
                if (nodeValue == "Add Field")
                {
                    node.SetValue(ReportDesignerTreeList.Columns.Count - 2, "  ");
                    node.SetValue(ReportDesignerTreeList.Columns.Count - 1, "Add Field");
                }
            }
        }

        private static int GetFirstColumnThatDoesntHaveData(TreeListNode node)
        {
            var index = 0;
            while (node.GetValue(index) as String != "Add Field")
            {
                index++;
            }
            return index;
        }

        private bool UserDidNotClickOnAddingCell(TreeListHitInfo info)
        {
            var value = info.Node.GetValue(info.Column) as string;


            return value != "Add Field";

        }

        private static void ButtonDragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        private void UpdateDragCursor(object sender, GiveFeedbackEventArgs e)
        {
            var info = ReportDesignerTreeList.CalcHitInfo(ReportDesignerTreeList.PointToClient(MousePosition));
            e.UseDefaultCursors = false;

            if (info.HitInfoType == HitInfoType.Cell &&
                LevelManagers[info.Node.Id].FieldCanBeAddedToLevel(SelectedDataClass) &&
                info.Node.GetValue(info.Column) != null)
            {
                Cursor.Current = DropOkCursor;
            }
            else
            {
                Cursor.Current = NoDropCursor;
            }
        }

        void TreeViewMouseDown(object sender, MouseEventArgs e)
        {
            var treeList = sender as TreeList;
            if (treeList == null)
                return;

            TreeListHitInfo hitInfo = treeList.CalcHitInfo(e.Location);

            Debug.WriteLine(hitInfo.HitInfoType.ToString());
            Debug.WriteLine(hitInfo.Column);

            if (e.Button == MouseButtons.Left && hitInfo.HitInfoType == HitInfoType.Cell)
            {
                var selectedDataClass = treeList.GetDataRecordByNode(hitInfo.Node);

                Debug.WriteLine(selectedDataClass);

                SelectedDataClass = selectedDataClass as DataClass;

                NoDropCursor = GetNoDragCursor(SelectedDataClass);
                DropOkCursor = GetDropOkCursor(SelectedDataClass);

                treeList.DoDragDrop(SelectedDataClass, DragDropEffects.Copy);
            }
        }

        private void AddItemsToTreeList(TreeList treeList, List<DataClass> source, string dataSetName, Color backColor)
        {
            treeList.DataSource = source;
            treeList.Columns[0].Caption = dataSetName + " Dataset";
            treeList.Columns[0].AppearanceCell.BackColor = backColor;
            treeList.Columns[0].OptionsColumn.AllowEdit = false;
            treeList.Columns[0].OptionsColumn.ReadOnly = true;
            treeList.Columns[1].VisibleIndex = -1;
            treeList.Columns[2].VisibleIndex = -1;
            treeList.Columns[3].VisibleIndex = -1;
            treeList.Columns[4].VisibleIndex = -1;

            treeList.Appearance.FocusedCell.BackColor = backColor;

        }

        public List<DataClass> DataSet2Source
        {
            get
            {
                var dataClassList = new List<DataClass>{
                new DataClass
            {
                Name = "Investment Code",
                Caption = "Investment",
                Id = 123214,
                IsVisible = true,
                AssociatedDataSet = "Investment"
            },
               new DataClass
            {
                Name = "Portfolio Code",
                Caption = "Portfolio",
                Id = 123214,
                IsVisible = true,
                AssociatedDataSet = "Investment"
            },
                
                 new DataClass
                {
                    Name = "YTD P&L Total",
                    Caption = "Year to Date P&L",
                    Id = 324234,
                    IsVisible = false,
                    AssociatedDataSet = "Investment"
                },
                new DataClass
                {
                    Name = "QTD P&L Total",
                    Caption = "Quarter to Date P&L",
                    Id = 324234,
                    IsVisible = false,
                    AssociatedDataSet = "Investment"
                },
                new DataClass
                {
                    Name = "MTD P&L Total",
                    Caption = "Month to Date P&L",
                    Id= 1324234,
                    IsVisible = true,
                    AssociatedDataSet = "Investment"
                },
                new DataClass
                {
                    Name = "DTD P&L Total",
                    Caption = "Daily P&L",
                    Id = 324234,
                    IsVisible = false,
                    AssociatedDataSet = "Investment"
                }
            };

                return dataClassList;
            }
        }

        }

    }
