﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DragAndDropTest
{
    public class LevelManager
    {
        public bool IsEmpty {
            get { return NumItems == 0; }
        }

        public int NumItems { get; set; }

        public int Level;

        public string AssociatedDataSet { get; set; }

        public LevelManager(int level)
        {
            Level = level;
            AssociatedDataSet = null;

        }

        public bool FieldCanBeAddedToLevel(DataClass data)
        {
            if (AssociatedDataSet != null) return AssociatedDataSet == data.AssociatedDataSet;
            AssociatedDataSet = data.AssociatedDataSet;
            return true;
        }
    }
}
