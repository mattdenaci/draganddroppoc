﻿namespace DragAndDropTest
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.DataSet2TreeList = new DevExpress.XtraTreeList.TreeList();
            this.propertyGridControl1 = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.NameRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.Id = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.ReportDesignerTreeList = new DevExpress.XtraTreeList.TreeList();
            this.DataSet1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.PropertyGridControlLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.ReportDesignerImageCollection = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataSet2TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertyGridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportDesignerTreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataSet1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PropertyGridControlLayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportDesignerImageCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.DataSet2TreeList);
            this.layoutControl1.Controls.Add(this.propertyGridControl1);
            this.layoutControl1.Controls.Add(this.ReportDesignerTreeList);
            this.layoutControl1.Controls.Add(this.DataSet1TreeList);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(964, 552);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // DataSet2TreeList
            // 
            this.DataSet2TreeList.Location = new System.Drawing.Point(12, 277);
            this.DataSet2TreeList.Name = "DataSet2TreeList";
            this.DataSet2TreeList.OptionsView.ShowIndicator = false;
            this.DataSet2TreeList.Size = new System.Drawing.Size(293, 263);
            this.DataSet2TreeList.TabIndex = 9;
            // 
            // propertyGridControl1
            // 
            this.propertyGridControl1.Location = new System.Drawing.Point(309, 443);
            this.propertyGridControl1.Name = "propertyGridControl1";
            this.propertyGridControl1.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.NameRow,
            this.Id});
            this.propertyGridControl1.Size = new System.Drawing.Size(643, 97);
            this.propertyGridControl1.TabIndex = 8;
            // 
            // NameRow
            // 
            this.NameRow.Name = "NameRow";
            this.NameRow.Properties.Caption = "Name";
            this.NameRow.Properties.FieldName = "Name";
            // 
            // Id
            // 
            this.Id.Name = "Id";
            this.Id.Properties.Caption = "Id";
            this.Id.Properties.FieldName = "Id";
            // 
            // ReportDesignerTreeList
            // 
            this.ReportDesignerTreeList.Location = new System.Drawing.Point(309, 12);
            this.ReportDesignerTreeList.Name = "ReportDesignerTreeList";
            this.ReportDesignerTreeList.OptionsView.ShowIndicator = false;
            this.ReportDesignerTreeList.Size = new System.Drawing.Size(643, 427);
            this.ReportDesignerTreeList.TabIndex = 7;
            // 
            // DataSet1TreeList
            // 
            this.DataSet1TreeList.Location = new System.Drawing.Point(12, 12);
            this.DataSet1TreeList.Name = "DataSet1TreeList";
            this.DataSet1TreeList.OptionsView.ShowIndicator = false;
            this.DataSet1TreeList.Size = new System.Drawing.Size(293, 261);
            this.DataSet1TreeList.TabIndex = 6;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.PropertyGridControlLayoutItem});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(964, 552);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.DataSet1TreeList;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(297, 265);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.ReportDesignerTreeList;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(297, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(647, 431);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.DataSet2TreeList;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 265);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(297, 267);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // PropertyGridControlLayoutItem
            // 
            this.PropertyGridControlLayoutItem.Control = this.propertyGridControl1;
            this.PropertyGridControlLayoutItem.CustomizationFormText = "PropertyGridControlLayoutItem";
            this.PropertyGridControlLayoutItem.Location = new System.Drawing.Point(297, 431);
            this.PropertyGridControlLayoutItem.Name = "PropertyGridControlLayoutItem";
            this.PropertyGridControlLayoutItem.Size = new System.Drawing.Size(647, 101);
            this.PropertyGridControlLayoutItem.Text = "PropertyGridControlLayoutItem";
            this.PropertyGridControlLayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.PropertyGridControlLayoutItem.TextToControlDistance = 0;
            this.PropertyGridControlLayoutItem.TextVisible = false;
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // ReportDesignerImageCollection
            // 
            this.ReportDesignerImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ReportDesignerImageCollection.ImageStream")));
            this.ReportDesignerImageCollection.InsertGalleryImage("database_16x16.png", "images/data/database_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/data/database_16x16.png"), 0);
            this.ReportDesignerImageCollection.Images.SetKeyName(0, "database_16x16.png");
            // 
            // MainForm
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(964, 552);
            this.Controls.Add(this.layoutControl1);
            this.Name = "MainForm";
            this.Text = "MainForm";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataSet2TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.propertyGridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportDesignerTreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataSet1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PropertyGridControlLayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportDesignerImageCollection)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraTreeList.TreeList DataSet1TreeList;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraVerticalGrid.PropertyGridControl propertyGridControl1;
        private DevExpress.XtraTreeList.TreeList ReportDesignerTreeList;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem PropertyGridControlLayoutItem;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow NameRow;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow Id;
        private DevExpress.XtraTreeList.TreeList DataSet2TreeList;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.Utils.ImageCollection ReportDesignerImageCollection;

    }
}