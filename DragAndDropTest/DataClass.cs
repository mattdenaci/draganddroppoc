﻿using System;

namespace DragAndDropTest
{
    public class DataClass
    {
        public String Name { get; set; }

        public String Caption { get; set; }

        public int Id { get; set; }

        public bool IsVisible { get; set; }
        
        public string AssociatedDataSet { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}